---
layout: post
title: Financial Assistance now available
card: financial_assistance.3fd3459b.png
---

<p class="lead">Talk to us if you need financial assistance to attend linux.conf.au 2021.</p>

Happy New Year!

There are now less than three weeks to go until linux.conf.au 2021. Gulp!

Since the conference is running online with no need for travel or accommodation costs, we hope this makes it easier for more people to attend.
Yet, we know 2020 was a challenge for many people financially; and some may still struggle to find the means to join us.

So we have some good news!
Thanks to the support of our Outreach and Inclusion sponsor, [Arm](/sponsors/), we have funds set aside to provide financial assistance to linux.conf.au attendees who might need it.
This program is part of our outreach and inclusion efforts for linux.conf.au.

To find out more about our financial assistance and how to apply, read our [Assistance](/attend/assistance/) page.
