---
layout: page
title: Proposals
permalink: /programme/proposals/
card: sessions.0b1adf3f.png
sponsors: true
---

## Important Information

 * Call for Proposals Opens: 15 October 2020
 * Call for Proposals Closes: ~~6 November 2020~~ 13 November 2020 [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
 * First round of acceptances: starting 23 November 2020
 * Conference Opens: 23 January 2021

## How to submit

**Our call for sessions and miniconfs is now closed.**
Thank you to everyone who submitted a talk or miniconf proposal.
Notifications from the sessions committee will be sent out in November 2020.

If you would like to review your proposals, you can continue to do so via your [Dashboard](/dashboard/).

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

## About linux.conf.au

linux.conf.au is a conference where people gather to learn about the entire world of Free and Open Source Software, directly from the people who contribute.
Many of these contributors give scheduled presentations, but much interaction occurs in-between and after formal sessions between all attendees.
Our aim is to create a deeply technical conference where we bring together industry leaders and experts on a wide range of subjects.

linux.conf.au welcomes submissions from first-time and seasoned speakers, from all free and open technology communities, and all walks of life.
We respect and encourage diversity at our conference.

## Conference Theme

Our theme is "So what's next?".

We all know we're living through unprecedented change and uncertain times.
How can open source play a role in creating, helping and adapting to this ongoing change?
What new developments in software and coding can we look forward to in 2021 and beyond?

Please let this inspire you, but not restrict you - we will still have many talks about other interesting things in our community.

Deciding what to speak about at linux.conf.au can be a tough challenge, particularly for new speakers.
If you would like some ideas on how to write a talk and how to submit a proposal, we recommend watching E. Dunham's [You Should Speak](https://www.youtube.com/watch?v=3QIQNcGnXes) talk from linux.conf.au 2018.

## Conference Location

linux.conf.au 2021 will be a fully online, virtual experience.
This means our speakers will be beaming in from their own homes or workplaces.
The organising team will be able to help speakers with their tech set-up.
Each accepted presenter will have a tech check prior to the event to smooth out any difficulties and there will be an option to pre-record the presentations.

## Proposal Types

We are accepting submissions for two different types of proposal:

 * Talk (45 minutes): These are generally presented in lecture format and form the bulk of the available conference slots.
 * Miniconf (full day): Single-track mini-conferences that run for the duration of a day on Saturday. We provide the room, and you provide the speakers. Together, you can explore a field in Open Source technologies in depth.

## Proposer Recognition

In recognition of the value that presenters and miniconf organisers bring to our conference, once a proposal is accepted, one presenter or organiser per proposal is entitled to:

 * Free registration, which holds all of the benefits of a Professional delegate ticket
 * Optionally, recognition as a Contributor, available at 50% off the advertised price

If your proposal includes more than one presenter or organiser, these additional people will be entitled to:

 * Professional or hobbyist registration at the Early Bird rate, regardless of whether the Early Bird rate is generally available

**Important Note for miniconf organisers**: These discounts apply to the organisers only.
Participants in your miniconf are not automatically given a ticket to the conference.
Each accepted Miniconf will be allocated a number of tickets to assign to their speakers as they deem appropriate.
Note that this may not cover all speakers for a miniconf, depending on the number of accepted speakers.
All participants in your miniconf must have a valid ticket to the conference or they will not be able to attend!

As a volunteer-run non-profit conference, linux.conf.au does not pay speakers to present at the conference.

## Accessibility

linux.conf.au aims to be accommodating to everyone who wants to attend or present at the conference.
We recognise that some people face accessibility challenges.
If you have special accessibility requirements, you can provide that information when submitting your proposal so that we can plan to properly accommodate you.

## Code of Conduct

By agreeing to present at or attend the conference you are agreeing to abide by the [terms and conditions](/attend/terms-and-conditions/).
We require all speakers and delegates to have read, understood, and act according to the standards set forth in our [Code of Conduct](/attend/code-of-conduct/).

## Recording

To increase the number of people that can view your presentation, linux.conf.au will record your talk and make it publicly available after the event.
We plan to release recordings of every talk at the conference under a [Creative Commons Attribution-ShareAlike Licence](https://creativecommons.org/licenses/by-sa/4.0/).
When submitting your proposal you may note that you do not wish to have your talk released, although we prefer and encourage all presentations to be recorded.

## Licensing

If the subject of your presentation is software, you must ensure the software has an Open Source Initiative-approved licence at the time of the close of our Call for Sessions.

## FAQ

**What is the difference between private and public summary?**

Private Summary is the portion that will be shown to the review team.
Give some more details of the talk and why it should be chosen.

Public Summary is the portion that will be shown to everyone on the website once the schedule is announced.
Provide enough details of the talk to whet the appetite.
