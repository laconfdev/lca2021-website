---
layout: page
title: Kernel Miniconf
permalink: /programme/miniconfs/kernel/
sponsors: true
---

<p class="lead">
Organised by Andrew Donnellan
</p>

## When

Saturday 23 January 2021

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="kernel-miniconf" day="saturday" closed="True" %}

## About

The Kernel Miniconf is a single-day Miniconf track about everything related to the kernel and low-level systems programming.

The Kernel Miniconf will focus on a variety of kernel-related topics - technical presentations on up-and-coming kernel developments, the future direction of the kernel, and kernel development community and process matters.
Past Kernel Miniconfs have included technical talks on topics such as memory management, RCU, scheduling, testing and filesystems, as well as talks on Linux kernel community topics such as licensing and Linux kernel development process.

We invite submissions on anything related to kernel and low-level systems programming.
We welcome submissions from developers of all levels of experience in the kernel community, covering a broad range of topics.
The focus of the Miniconf will primarily be on Linux, however non-Linux talks of sufficient interest to a primarily Linux audience will be considered.
