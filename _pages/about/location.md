---
layout: page
title: Location
permalink: /about/location/
sponsors: true
---

## The Venue

linux.conf.au 2021 will not have a fixed venue in 2021, as it will be delivered in an online format to everyone worldwide.

We will be using [Venueless](https://venueless.org) as the platform for the conference.
All delegates will be able to login to the platform via a link from their [Dashboard](/dashboard/) once the conference has started.
As is tradition, boarding passes will be sent out in the week prior to the conference with full details on how to access everything.

If you have not yet purchased your [ticket](/attend/tickets/), you can do so via the [Dashboard](/dashboard/).

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Buy a Ticket</a>
